#define _USE_MATH_DEFINES

#include <glad/glad.h> 
#include <GLFW\glfw3.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include "vec3.cpp"

#include <ctime>
#include <cmath>
#include <iostream>
#include <vector>

const double PI = 3.14159265358979323846;

float waveLengthFrom = 0.3;
float waveLengthTo = 12.5;
float airTemp = 300;
float airEffect = 0.005;
float radiationDistanceSensitivity = 1.5;
float viewerDistanceSnsitivity = 0.005;

const vec3 viewerPosition = vec3(2.0f, 2.0f, 12.0f);
const bool rotate = false;

int iteration = 0;
float averageFrameRate = 0;
float startTime = 0;
float endTime = 0;

/*
0.3 - 1.3 : 63
1.5 - 1.8 : 80
2.0 - 2.6 : 80
2.9 - 3.0 : 30
3.0 - 3.3 : 60
3.3 - 4.3 : 90
4.5 - 5.0 : 65
5.0 - 5.5 : 30
7.5 - 8.0 : 25
8.0 - 10.5 : 80
10.5 - 12.0 : 75
12.0 - 12.5 : 73
*/
const std::vector<int> atmosphereTransmittance = { 63, 80, 80, 30, 60, 90, 65, 30, 25, 80, 75, 73 };
const std::vector<float> spectralStarts = { 0.3, 1.5, 2, 2.9, 3, 3.3, 4.5, 5, 7.5, 8, 10.5, 12 };
const std::vector<float> spectralEnds = { 1.3, 1.8, 2.6, 3, 3.3, 4.3, 5, 5.5, 8, 10.5, 12, 12.5 };

//Even numbers
const int LOD = 6;
const int levels = 2 * LOD + 1;
const float radius = 1;
const int numberOfVertices = 2 + levels * (levels - 2);
const int numberOfPolygons = (levels - 2) * 2 * levels;

const int sphereVSize = numberOfVertices * 4;
const int sphereISize = numberOfPolygons * 3;

float sphereV[sphereVSize];
unsigned int sphereI[sphereISize];

const int numberOfSpheres = 4;

float sphereVertices[sphereVSize * numberOfSpheres];
unsigned int sphereIndices[sphereISize * numberOfSpheres];

std::vector<std::vector<double>> intesityControl = {};
std::vector<float> dQControl = {};

std::vector<bool> viewerVisibleVertices = {};

enum MaterialType {
	WOOD, MARBLE, BRICK
};

struct Material {
	// g
	float mass;
	// cm^3
	float volume = 1;
	// g cm^-3
	float density;
	// J g^-1 K^-1
	float heatCap;

	std::vector<float> emissivity;
	std::vector<float> reflectance;

	MaterialType type;

public:
	Material(MaterialType _type) {
		if (_type == WOOD) {
			type = _type;
			mass = 0.6;
			density = 0.6;
			heatCap = 2;
			emissivity = { 51.91, 59.65, 35.86, 3.55, 6.67, 13.12, 19.52, 21.71, 3.3, 4.54, 7.93, 6.25 };
			reflectance = { 100-51.91, 100-59.65, 100-35.86, 100-3.55, 100-6.67, 100-13.12, 100-19.52, 100-21.71, 100-3.3, 100-4.54, 100-7.93, 100-6.25};
		}
		if (_type == MARBLE) {
			type = _type;
			mass = 2.711;
			density = 2.711;
			heatCap = 0.95;
			emissivity = { 71.78, 83.11, 34.37, 6.65, 5.47, 4, 3.82, 3.65, 7.98, 7.22, 7.03, 5.6 };
			reflectance = { 100-71.78, 100-83.11, 100-34.37, 100-6.65, 100-5.47, 100-4, 100-3.82, 100-3.65, 100-7.98, 100-7.22, 100-7.03, 100-5.6 };
		}
		if (_type == BRICK) {
			type = _type;
			mass = 1.922;
			density = 1.922;
			heatCap = 0.9;
			emissivity = { 15, 56.6, 52.89, 14.7542, 22.28, 36.94, 19.36, 6.73, 1.33, 6, 2.17, 1.89 };
			reflectance = { 100-15, 100-56.6, 100-52.89, 100-14.7542, 100-22.28, 100-36.94, 100-19.36, 100-6.73, 100-1.33, 100-6, 100-2.17, 100-1.89 };
		}
	}
};

Material materialWood = Material(WOOD);
Material materialBrick = Material(BRICK);
Material materialMarble = Material(MARBLE);

class Sphere {
public:
	vec3 center;
	float radius;
	int LOD;
	float startTemp;
	Material* material;
	float ks;
	float smoothness;

public:
	Sphere(const vec3& _center, float _radius, int _LOD, Material* _material, float _startTemp, float _ks, float _smoothness) {
		center = _center; 
		radius = _radius; 
		LOD = _LOD;
		material = _material;
		startTemp = _startTemp;
		ks = _ks;
		smoothness = _smoothness;
	}
};

struct VertexInfo {
	int vertexIndex;
	int sphereIndex;
	float ks;
	float smoothness;
	float temp;

	vec3 position;
	vec3 normal;
	Material* material;

	std::vector<int> visibleVerticesIndexes;
	std::vector<float> pastGrayBodyRadianceOut = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
	std::vector<float> grayBodyRadianceOut = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };

	VertexInfo(int _vertexIndex, int _sphereIndex, Material* _material, vec3 _center, float x, float y, float z, float _temp, float _ks, float _smoothness) {
		vertexIndex = _vertexIndex;
		sphereIndex = _sphereIndex;
		material = _material;
		position.x = x;
		position.y = y;
		position.z = z;
		normal = position - _center;
		temp = _temp;
		ks = _ks;
		smoothness = _smoothness;
	}

	VertexInfo(vec3 _position) {
		position = _position;
	}

	void setTemp(float _temp) {
		temp = _temp;
	}

	std::vector<float> calculateBBRadiance(float temp) {
		float waveLength = waveLengthFrom;
		std::vector<float> bbSpectralRadiance = {};

		for (int i = 0; i < 12; ++i) {
			float bbRadiance = 0;

			if (spectralStarts.at(i) >= waveLengthFrom && spectralEnds.at(i) <= waveLengthTo) {
				for (waveLength = spectralStarts.at(i); waveLength <= spectralEnds.at(i); waveLength += 0.1) {
					bbRadiance += 374180000 / (pow(waveLength, 5) * (exp(14388 / (waveLength * temp)) - 1));
				}
			}
			float radianceScale = ((spectralEnds.at(i) - spectralStarts.at(i)) / (waveLengthTo - waveLengthFrom)) / numberOfVertices;
			bbSpectralRadiance.push_back(bbRadiance * radianceScale);
		}
		return bbSpectralRadiance;
	}

	void updateTemp(float dQ) {
		temp += dQ / (material->mass * material->heatCap);
		pastGrayBodyRadianceOut = grayBodyRadianceOut;
		std::vector<float> bbRad = calculateBBRadiance(temp);
		for (int i = 0; i < grayBodyRadianceOut.size(); ++i) {
			grayBodyRadianceOut.at(i) = material->emissivity.at(i) / 100 * bbRad.at(i);
		}
	}
};


//---- Declarations ->

void discoverVisibleVertecies();
void generateSphere(glm::vec3 center, int radius, int levelOfDetail, float* v, unsigned int* i);
std::vector<float> getSourceToTargetRadinace(VertexInfo* sourceVertex, VertexInfo* targetVertex);
double brdf(VertexInfo* reflectingVertex, vec3& emitingVertexPosition);
std::vector<float> radianceTroughAtmosphere(std::vector<float>& radiation, vec3& vertexPosition);

//----------

std::vector<VertexInfo> allVertices = {};

unsigned int vertexShader;
unsigned int fragmentShader;
unsigned int shaderProgram;
unsigned int VAO;
unsigned int VBO;
unsigned int EBO;

const char* fragmentShaderSource =
"#version 330 core\n"
"in vec4 vertexColor;\n"
"\n"
"out vec4 FragColor;\n"
"void main() {\n"
"	FragColor = vertexColor;\n"
"}\0";

const char* vertexShaderSource =
"#version 330 core\n"
"layout (location = 0) in vec3 position;\n"
"layout (location = 1) in float intensity;\n"
"\n"
"out vec4 vertexColor;\n"
"\n"
"uniform mat4 model;\n"
"uniform mat4 view;\n"
"uniform mat4 projection;\n"
"\n"
"void main() {\n"
"   gl_Position = projection * view * model * vec4(position, 1.0f);\n"
"   vertexColor = vec4(intensity, 0.0f, 1.0f - intensity, 1.0f);\n"
"}\0";


//---- GL functions ->

void framebuffer_size_callback(GLFWwindow* window, int width, int height) {
	glViewport(0, 0, width, height);
}

void processInput(GLFWwindow* window) {
	if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
		glfwSetWindowShouldClose(window, true);
}

void renderingCommands() {
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glUseProgram(shaderProgram);

	iteration++;
	float time = glfwGetTime();

	float minIntensity = INFINITY;
	int minIndex = 0;

	float maxIntensity = 0;
	int maxIndex = 0;
	
	for (int i = 0; i < allVertices.size(); i++) {

		std::vector<float> absorbedRadiance = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
		std::vector<float> reflectedRadiance = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };

		std::vector<float> dRadiance = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };

		VertexInfo vertex = allVertices.at(i);

		for (int j = 0; j < vertex.visibleVerticesIndexes.size(); j++) {
			VertexInfo visibleVertex = allVertices.at(vertex.visibleVerticesIndexes.at(j));
			std::vector<float> vertexToVertexRadiance = getSourceToTargetRadinace(&visibleVertex, &vertex);

			float BRDF = viewerVisibleVertices.at(i) ? brdf(&vertex, visibleVertex.position) : 0;
			for (int k = 0; k < 12; ++k) {
				absorbedRadiance.at(k) = absorbedRadiance.at(k) + vertex.material->emissivity.at(k) * vertexToVertexRadiance.at(k);
				reflectedRadiance.at(k) = reflectedRadiance.at(k) + vertex.material->reflectance.at(k) * BRDF * vertexToVertexRadiance.at(k);
			}
		}

		for (int j = 0; j < 12; ++j) {
			dRadiance.at(j) = absorbedRadiance.at(j) - vertex.pastGrayBodyRadianceOut.at(j);
		}

		float dQ = 0;
		for (int j = 0; j < 12; ++j) {
			dQ += dRadiance.at(j);
		}

		dQControl.push_back(dQ);

		VertexInfo viewer = VertexInfo(viewerPosition);
		std::vector<float> vertexViewerRadiance = getSourceToTargetRadinace(&vertex, &viewer);
		std::vector<float> reflectedRadianceToViewer = radianceTroughAtmosphere(reflectedRadiance, vertex.position);

		float intensityToViewer = 0;
		for (int j = 0; j < 12; ++j) {
			intensityToViewer += vertexViewerRadiance.at(j) + reflectedRadianceToViewer.at(j);
		}

		sphereVertices[vertex.vertexIndex * 4 + 3] = intensityToViewer;

		if (intensityToViewer < minIntensity) {
			minIntensity = intensityToViewer;
			minIndex = i;
		}

		if (intensityToViewer > maxIntensity) {
			maxIntensity = intensityToViewer;
			maxIndex = i;
		}
	}

	for (int i = 0; i < allVertices.size(); ++i) {
		sphereVertices[i * 4 + 3] = (sphereVertices[i * 4 + 3] - minIntensity) / (maxIntensity - minIntensity);
	}

	std::cout << "Min temp: " << allVertices.at(minIndex).temp << " K " << "Max temp: " << allVertices.at(maxIndex).temp << " K\n";

	for (int i = 0; i < allVertices.size(); ++i) {
		allVertices.at(i).setTemp(allVertices.at(i).temp + airEffect * (airTemp - allVertices.at(i).temp));
		allVertices.at(i).updateTemp(dQControl.at(i));
	}

	dQControl = {};

	glm::mat4 view = glm::mat4(1.0f);
	// note that we're translating the scene in the reverse direction of where we want to move
	view = glm::translate(view, glm::vec3(-2.0f, -2.0f, -12.0f));
	if (rotate) {
		view = glm::lookAt(glm::vec3(sin(glfwGetTime()) * 10.0f, 0.0, cos(glfwGetTime()) * 10.0f), glm::vec3(0.0, 0.0, 0.0), glm::vec3(0.0, 1.0, 0.0));
	}
	int viewLoc = glGetUniformLocation(shaderProgram, "view");
	glUniformMatrix4fv(viewLoc, 1, GL_FALSE, glm::value_ptr(view));

	glm::mat4 projection;
	projection = glm::perspective(glm::radians(45.0f), 800.0f / 600.0f, 0.1f, 100.0f);
	int projectionLoc = glGetUniformLocation(shaderProgram, "projection");
	glUniformMatrix4fv(projectionLoc, 1, GL_FALSE, glm::value_ptr(projection));

	// Normal cube
	glm::mat4 model = glm::mat4(1.0f);
	if (rotate) {
		model = glm::translate(model, glm::vec3(0.0f, 0.0f, 0.0f));
		model = glm::rotate(model, (float)glfwGetTime(), glm::vec3(1.0f, 1.0f, 0.0f));
	}
	int modelLoc = glGetUniformLocation(shaderProgram, "model");
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));

	glBufferData(GL_ARRAY_BUFFER, sizeof(sphereVertices), sphereVertices, GL_DYNAMIC_DRAW);

	glUseProgram(shaderProgram);
	glBindVertexArray(VAO);
	glDrawElements(GL_TRIANGLES, sizeof(sphereIndices), GL_UNSIGNED_INT, 0);
	glBindVertexArray(0);
}

void loadAndAttachShaders() {
	// Load vertex Shader
	vertexShader = glCreateShader(GL_VERTEX_SHADER);
	glShaderSource(vertexShader, 1, &vertexShaderSource, NULL);
	glCompileShader(vertexShader);

	// Checke compilation result for vertex shader
	int  success;
	char infoLog[512];
	glGetShaderiv(vertexShader, GL_COMPILE_STATUS, &success);
	if (!success) {
		glGetShaderInfoLog(vertexShader, 512, NULL, infoLog);
		std::cout << "ERROR::SHADER::VERTEX::COMPILATION_FAILED\n" << infoLog << std::endl;
	}

	// Load fragment Shader
	fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(fragmentShader, 1, &fragmentShaderSource, NULL);
	glCompileShader(fragmentShader);

	// Checke compilation result for vertex shader
	glGetShaderiv(fragmentShader, GL_COMPILE_STATUS, &success);
	if (!success) {
		glGetShaderInfoLog(fragmentShader, 512, NULL, infoLog);
		std::cout << "ERROR::SHADER::FRAGMENT::COMPILATION_FAILED\n" << infoLog << std::endl;
	}

	// Attach shaders to program and link program
	shaderProgram = glCreateProgram();
	glAttachShader(shaderProgram, vertexShader);
	glAttachShader(shaderProgram, fragmentShader);
	glLinkProgram(shaderProgram);

	// Check compilation results for program
	glGetProgramiv(shaderProgram, GL_LINK_STATUS, &success);
	if (!success) {
		glGetProgramInfoLog(shaderProgram, 512, NULL, infoLog);
	}
	glUseProgram(shaderProgram);

	glDeleteShader(vertexShader);
	glDeleteShader(fragmentShader);
}

//-----------------------------------------------------------------

std::vector<Sphere> spheres = {};

void initScene() {

	generateSphere(glm::vec3{ 0.f, 0.f, 0.f }, radius, LOD, sphereV, sphereI);

	spheres = {
		Sphere(vec3{ 0.f, 0.f, 0.f }, radius, LOD, &materialBrick, 400.0f, 0.1, 1),
		Sphere(vec3{ 0.f, 4.f, 0.f }, radius, LOD, &materialMarble, 300.0f, 0.8, 8),
		Sphere(vec3{ 4.f, 0.f, 0.f }, radius, LOD, &materialWood, 500.0f, 0.5, 3),
		Sphere(vec3{ 3.f, 3.f, 2.f }, radius, LOD, &materialMarble, 450.0f, 0.9, 10)
	};

	for (int i = 0; i < spheres.size(); ++i) {
		vec3 center = spheres.at(i).center;

		for (int j = 0; j < numberOfVertices; ++j) {
			float x = sphereV[j * 4];
			float y = sphereV[j * 4 + 1];
			float z = sphereV[j * 4 + 2];

			sphereVertices[((i * numberOfVertices) + j) * 4] = x + center.x;
			sphereVertices[((i * numberOfVertices) + j) * 4 + 1] = y + center.y;
			sphereVertices[((i * numberOfVertices) + j) * 4 + 2] = z + center.z;

			allVertices.push_back(VertexInfo(((i * numberOfVertices) + j), i, spheres.at(i).material, spheres.at(i).center, x + center.x, y + center.y, z + center.z, spheres.at(i).startTemp, spheres.at(i).ks, spheres.at(i).smoothness));
		}

		for (int j = 0; j < sphereISize; ++j) {
			sphereIndices[i * sphereISize + j] = sphereI[j] + numberOfVertices * i;
		}

		for (int i = 0; i < allVertices.size(); ++i) {
			vec3 V = viewerPosition - allVertices.at(i).position;
			vec3 N = allVertices.at(i).normal;

			V.Normalize();
			N.Normalize();

			viewerVisibleVertices.push_back(dotprod(N, V) > 0);
		}
	}
}

int main() {

	char def;
	std::cout << "Default settings? y/n" << std::endl;
	std::cin >> def;
	if (def == 'n') {
		std::cout << "Wavelength from {0.3, 1.5, 2, 2.9, 3, 3.3, 4.5, 5, 7.5, 8, 10.5, 12} " << std::endl;
		std::cin >> waveLengthFrom;
		std::cout << "Wavelength to {1.3, 1.8, 2.6, 3, 3.3, 4.3, 5, 5.5, 8, 10.5, 12, 12.5} " << std::endl;
		std::cin >> waveLengthTo;

		char air;
		std::cout << "Include air effect? y/n" << std::endl;
		std::cin >> air;
		if (air == 'y') {
			std::cout << "Air temp in Kelvins " << std::endl;
			std::cin >> airTemp;
			std::cout << "Air effect " << std::endl;
			std::cin >> airEffect;
		} else  
			airEffect = 0;

		std::cout << "Extinction coefficient " << std::endl;
		std::cin >> radiationDistanceSensitivity;
	}

	initScene();
	discoverVisibleVertecies();

	int avgVisible = 0;
	for (int i = 0; i < allVertices.size(); ++i) {
		avgVisible += allVertices.at(i).visibleVerticesIndexes.size();
	}
	std::cout << "Average visivle vertices: " << avgVisible << std::endl;

	startTime = (float)glfwGetTime();

	glfwInit();
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

	GLFWwindow* window = glfwCreateWindow(800, 600, "OpenGL", NULL, NULL);
	if (window == NULL) {
		std::cout << "Failed to create GLFW window" << std::endl;
		glfwTerminate();
		return -1;
	}
	glfwMakeContextCurrent(window);

	if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress)) {
		std::cout << "Failed to initialize GLAD" << std::endl;
		return -1;
	}
	glfwSetFramebufferSizeCallback(window, framebuffer_size_callback);

	glEnable(GL_DEPTH_TEST);

	glGenVertexArrays(1, &VAO);
	glGenBuffers(1, &VBO);
	glGenBuffers(1, &EBO);

	// bind Vertex Array Object
	glBindVertexArray(VAO);
	// copy our vertices array in a vertex buffer for OpenGL to use
	glBindBuffer(GL_ARRAY_BUFFER, VBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(sphereVertices), sphereVertices, GL_DYNAMIC_DRAW);
	// copy our index array in a element buffer for OpenGL to use
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(sphereIndices), sphereIndices, GL_STATIC_DRAW);
	// then set the vertex attributes pointers
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 4 * sizeof(float), (GLvoid*)0);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(1, 1, GL_FLOAT, GL_FALSE, 4 * sizeof(float), (GLvoid*)(3 * sizeof(GLfloat)));
	glEnableVertexAttribArray(1);

	loadAndAttachShaders();

	// Render loop
	while (!glfwWindowShouldClose(window)) {

		// Inputs
		processInput(window);

		// Render commands
		renderingCommands();

		// check and call events and swap the buffers
		glfwSwapBuffers(window);
		glfwPollEvents();
	}
	glDeleteVertexArrays(1, &VAO);
	glDeleteBuffers(1, &EBO);
	glDeleteBuffers(1, &VBO);

	endTime = (float)glfwGetTime();

	//per sec
	averageFrameRate = iteration / (endTime - startTime);
	std::cout << averageFrameRate << " fps" << std::endl;

	std::cout << "LOD: " << LOD << std::endl;
	std::cout << "numberOfSpheres: " << numberOfSpheres << std::endl;
	std::cout << "waveLengthFrom: " << waveLengthFrom << std::endl;
	std::cout << "waveLengthTo: " << waveLengthTo << std::endl;
	std::cout << "numberOfVertices: " << numberOfVertices << std::endl;
	std::cout << "numberOfPolygons: " << numberOfPolygons << std::endl;

	glfwTerminate();
	return 0;
}

//---- Definitions ->

// Source vertex is radiation source
std::vector<float> getSourceToTargetRadinace(VertexInfo* sourceVertex, VertexInfo* targetVertex) {

	float distance = abs((sourceVertex->position - targetVertex->position).Length());
	std::vector<float> transmittedRadiance = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};

	for (int i = 0; i < 12; ++i) {
		if (spectralStarts.at(i) >= waveLengthFrom && spectralEnds.at(i) <= waveLengthTo) {
			if (targetVertex->position.z == viewerPosition.z)
				transmittedRadiance.at(i) = sourceVertex->grayBodyRadianceOut.at(i) * exp(-distance * viewerDistanceSnsitivity * radiationDistanceSensitivity * 100 / atmosphereTransmittance.at(i));
			else
				transmittedRadiance.at(i) = sourceVertex->grayBodyRadianceOut.at(i) * exp(-distance * radiationDistanceSensitivity * 100 / atmosphereTransmittance.at(i));
		}
	}
	return transmittedRadiance;
}

std::vector<float> radianceTroughAtmosphere(std::vector<float>& radiation, vec3& vertexPosition) {
	float distance = (vertexPosition - viewerPosition).Length();
	std::vector<float> transmittedRadiance = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };

	for (int i = 0; i < 12; ++i) {
		if (spectralStarts.at(i) >= waveLengthFrom && spectralEnds.at(i) <= waveLengthTo) {
			transmittedRadiance.at(i) = radiation.at(i) * exp(-distance * radiationDistanceSensitivity * 100 / atmosphereTransmittance.at(i));
		}
	}
	return transmittedRadiance;
}

double brdf(VertexInfo* reflectingVertex, vec3& emitingVertexPosition) {
	double ks = reflectingVertex->ks;
	double smoothness = reflectingVertex->smoothness;

	vec3 N = reflectingVertex->normal;
	N.Normalize();
	vec3 V = viewerPosition - reflectingVertex->position;
	V.Normalize();
	vec3 L = emitingVertexPosition - reflectingVertex->position;
	L.Normalize();
	vec3 H = L + V;
	H.Normalize();

	double F;
	double G;
	double D;

	double LdotH = dotprod(L, H);
	double NdotH = dotprod(N, H);
	double NdotL = dotprod(N, L);
	double NdotV = dotprod(N, V);

	double d = fmax((4 * NdotL * NdotV), 0.0001);

	F = ks + (1 - ks) * pow((1 - LdotH), 5);
	G = d / pow(LdotH, 2);
	D = fmax(NdotH, 0) * (smoothness + 2) / (2.0f * PI) * pow(NdotH, smoothness);

	return F * G * D / d;
}

void discoverVisibleVertecies() {
	for (int i = 0; i < allVertices.size(); i++) {
		VertexInfo v1 = allVertices.at(i);
		v1.updateTemp(0);

		for (int j = 0; j < allVertices.size(); j++) {
			VertexInfo v2 = allVertices.at(j);

			if (v1.sphereIndex != v2.sphereIndex) {

				for (int k = 0; k < numberOfSpheres; ++k) {

					if (k != v1.sphereIndex && k != v2.sphereIndex) {

						vec3 N1 = v1.normal;
						vec3 N2 = v2.normal;
						vec3 V21 = v2.position - v1.position;
						vec3 V12 = v1.position - v2.position;

						N1.Normalize();
						N2.Normalize();
						V21.Normalize();
						V12.Normalize();

						if (dotprod(V21, N1) > 0 && dotprod(V12, N2) > 0) {

							vec3 a = spheres.at(k).center - v1.position;
							vec3 b = v2.position - v1.position;

							a.Normalize();
							b.Normalize();

							float dot = dotprod(a, b);

							if (dot > 0) {
								float h = 2 * a.Length() * acosf(dot) / PI;
								if (spheres.at(k).radius < h || spheres.at(k).radius - h < 0.05f) {
									v1.visibleVerticesIndexes.push_back(v2.vertexIndex);
								}
							}
						}
					}
				}
			}
		}
		allVertices.at(i) = v1;
	}
}

void generateSphere(glm::vec3 center, int radius, int levelOfDetail, float* v, unsigned int* i) {
	float* vertices = v;
	unsigned int* indices = i;

	int verticesInLevel = 2 * levelOfDetail + 1;

	int verticesIndex = 0;
	int indicesIndex = 0;
	int vertexIndex = 1;
	int indicesIndexHalf = 0;

	glm::vec3 vertex = center - glm::vec3(0, radius, 0);

	vertices[verticesIndex++] = vertex.x;
	vertices[verticesIndex++] = vertex.y;
	vertices[verticesIndex++] = vertex.z;
	vertices[verticesIndex++] = 0;

	int currentLevel = 1;
	float yStep = 90.f / (float) levelOfDetail;
	float xStep = 360.f / (float) verticesInLevel;

	for (float lat = -90.f + yStep; lat < 90.f; lat = lat + yStep) {
		float shift = (currentLevel % 2) * (xStep / 2.f);

		for (float lon = shift; lon < 360.f + shift; lon = lon + xStep) {
			vertex = center + glm::vec3(
				radius * sin(lon * PI / 180.f) * cos(lat * PI / 180.f),
				radius * sin(lat * PI / 180.f),
				radius * cos(lon * PI / 180.f) * cos(lat * PI / 180.f)
			);

			vertices[verticesIndex++] = vertex.x;
			vertices[verticesIndex++] = vertex.y;
			vertices[verticesIndex++] = vertex.z;
			vertices[verticesIndex++] = 0;
		}
		currentLevel++;
	}

	vertex = center + glm::vec3(0, radius, 0);

	vertices[verticesIndex++] = vertex.x;
	vertices[verticesIndex++] = vertex.y;
	vertices[verticesIndex++] = vertex.z;
	vertices[verticesIndex++] = 0;


	int numberOfVertices = 2 + verticesInLevel * (verticesInLevel - 2);

	indices[indicesIndex++] = 0;
	indices[indicesIndex++] = verticesInLevel;
	indices[indicesIndex++] = 1;

	for (int i = 1; i < verticesInLevel; ++i) {
		indices[indicesIndex++] = 0;
		indices[indicesIndex++] = i;
		indices[indicesIndex++] = i + 1;
	}

	for (int vertexIndex = 1; vertexIndex < 1 + verticesInLevel * (verticesInLevel - 3); ++vertexIndex) {
		int levelStart = vertexIndex - ((vertexIndex - 1) % verticesInLevel);

		if (div((levelStart - 1), verticesInLevel).quot % 2 == 0) {
			indices[indicesIndex++] = vertexIndex;
			indices[indicesIndex++] = vertexIndex + verticesInLevel;
			indices[indicesIndex++] = levelStart + verticesInLevel + (vertexIndex % verticesInLevel);

			indices[indicesIndex++] = levelStart + (vertexIndex % verticesInLevel);
			indices[indicesIndex++] = vertexIndex;
			indices[indicesIndex++] = levelStart + verticesInLevel + (vertexIndex % verticesInLevel);
		} else {
			indices[indicesIndex++] = levelStart + (vertexIndex % verticesInLevel);
			indices[indicesIndex++] = vertexIndex;
			indices[indicesIndex++] = vertexIndex + verticesInLevel;

			indices[indicesIndex++] = vertexIndex + verticesInLevel;
			indices[indicesIndex++] = vertexIndex;
			indices[indicesIndex++] = levelStart + verticesInLevel + ((vertexIndex - 2) % verticesInLevel);
		}
	}

	indices[indicesIndex++] = numberOfVertices - verticesInLevel - 1;
	indices[indicesIndex++] = numberOfVertices - 2;
	indices[indicesIndex++] = numberOfVertices - 1;

	for (int i = 1; i < verticesInLevel; ++i) {
		indices[indicesIndex++] = numberOfVertices - i - 1;
		indices[indicesIndex++] = numberOfVertices - i - 2;
		indices[indicesIndex++] = numberOfVertices - 1;
	}

}

