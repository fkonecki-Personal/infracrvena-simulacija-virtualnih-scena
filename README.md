# Infracrvena Simulacija Virtualnih Scena
Projekt sadrži prezentaciju i diplomski rad na temu "Infracrvena Simulacija Virtualnih Scena", te praktični dio diplomskog rada u kojime je implementirana simulacija prijenosa topline između objekata u sceni, prikazana u infracrvenom spektru.

<img src="/Screenshots/Main.png" alt="Screenshot" width="450" height="450">
